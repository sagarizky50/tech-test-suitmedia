# Technical Test Suitmedia

## Name
Suitmedia Technical Test

## Description
Technical Test for the Frontend Engineer Intern position at Magang Merdeka Batch 5 with Suitmedia! This website has been developed using the React.js framework and styled using CSS.

## Installation
```
git clone https://gitlab.com/sagarizky50/tech-test-suitmedia.git
cd directory
npm install
npm run start
```

## Authors
Rizky Akbar Asmaran

## Project status
Done 
