import React from 'react';
import '../style/__Values.css';
import inovativeLogo from '../asset/lightbulb-o.png';
import loyaltyLogo from '../asset/bank.png';
import respectLogo from '../asset/balance-scale.png';
import Card from './Card';

const Values = () => {
  return (
    <>
      <h1>OUR VALUES</h1>
      <section className="values">
        <Card
          image={inovativeLogo}
          title="INNOVATIVE"
          description="Proin pharetra rhoncus tortor, tincidunt vehicula elit blandit at. Nullam elementum rutrum lacus, id aliquam odio tempus sed. In at posuere augue. Vestibulum mattis commodo quam, sed laoreet purus."
        />
        <Card
          image={loyaltyLogo}
          title="LOYALTY"
          description="Proin pharetra rhoncus tortor, tincidunt vehicula elit blandit at. Nullam elementum rutrum lacus, id aliquam odio tempus sed. In at posuere augue. Vestibulum mattis commodo quam, sed laoreet purus."
        />
        <Card
          image={respectLogo}
          title="RESPECT"
          description="Proin pharetra rhoncus tortor, tincidunt vehicula elit blandit at. Nullam elementum rutrum lacus, id aliquam odio tempus sed. In at posuere augue. Vestibulum mattis commodo quam, sed laoreet purus."
        />
      </section>
    </>
  );
};

export default Values;
