import React from 'react';
import '../style/__Footer.css';
import twitter from '../asset/twitter.png';
import facebook from '../asset/facebook-official.png';

const Footer = () => {
  return (
    <div className="footer">
      <div className="footer-content">
        <span className="footer-copyright">
          Copyright @ 2016. PT.Suitmedia
        </span>
        <div className="footer-icons">
          <a href="https://www.facebook.com/suitmedia/" target="_blank" rel="noopener noreferrer">
            <img src={facebook} alt="Facebook" className="footer-icon" />
          </a>
          <a href="https://twitter.com/suitmedia" target="_blank" rel="noopener noreferrer">
            <img src={twitter} alt="Twitter" className="footer-icon" />
          </a>
        </div>
      </div>
    </div>
  );
};

export default Footer;
