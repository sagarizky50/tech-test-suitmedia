// ContactForm.js

import React, { useState } from 'react';
import '../style/__ContactForm.css';

const ContactForm = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [message, setMessage] = useState('');
  const [isFormSubmitted, setIsFormSubmitted] = useState(false);
  const [errors, setErrors] = useState({
    name: '',
    email: '',
    message: ''
  });

  const handleSubmit = (e) => {
    e.preventDefault();

    let isFormValid = true;
    const newErrors = {
      name: '',
      email: '',
      message: ''
    };

    // Check for empty fields
    if (name.trim() === '') {
      newErrors.name = 'Name is required';
      isFormValid = false;
    }
    if (email.trim() === '') {
      newErrors.email = 'Email is required';
      isFormValid = false;
    }
    if (message.trim() === '') {
      newErrors.message = 'Message is required';
      isFormValid = false;
    }

    // Check for valid email
    if (email.trim() !== '' && (!email.includes('@') || !email.includes('.'))) {
      newErrors.email = 'Please enter a valid email address';
      isFormValid = false;
    }

    if (isFormValid) {
      setIsFormSubmitted(true);
      setName('');
      setEmail('');
      setMessage('');
      setErrors({
        name: '',
        email: '',
        message: ''
      });
    } else {
      setErrors(newErrors);
    }
  };

  const handleInputChange = (e, field) => {
    if (errors[field] !== '') {
      setErrors((prevErrors) => ({ ...prevErrors, [field]: '' }));
    }

    if (field === 'name') {
      setName(e.target.value);
    } else if (field === 'email') {
      setEmail(e.target.value);
    } else if (field === 'message') {
      setMessage(e.target.value);
    }
  };

  const handleBlur = (field) => {
    if (field === 'name' && name.trim() === '') {
      setErrors((prevErrors) => ({ ...prevErrors, name: 'Name is required' }));
    }
    if (field === 'email' && email.trim() === '') {
      setErrors((prevErrors) => ({ ...prevErrors, email: 'Email is required' }));
    }
    if (field === 'email' && email.trim() !== '' && (!email.includes('@') || !email.includes('.'))) {
      setErrors((prevErrors) => ({ ...prevErrors, email: 'Please enter a valid email address' }));
    }
    if (field === 'message' && message.trim() === '') {
      setErrors((prevErrors) => ({ ...prevErrors, message: 'Message is required' }));
    }
  };

  return (
    <section className="contact">
      <h2>CONTACT US</h2>
      {isFormSubmitted ? (
        <h1>Thank you for your message!</h1>
      ) : (
        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <label htmlFor="name">Name:</label>
            <input
              type="text"
              id="name"
              placeholder="John Doe"
              value={name}
              onChange={(e) => handleInputChange(e, 'name')}
              onBlur={() => handleBlur('name')}
              className={errors.name !== '' ? 'error' : ''}
            />
            {errors.name !== '' && <span className="error-message">{errors.name}</span>}
          </div>
          <div className="form-group">
            <label htmlFor="email">Email:</label>
            <input
              type="email"
              id="email"
              placeholder="sukamajuahay@gmail.com"
              value={email}
              onChange={(e) => handleInputChange(e, 'email')}
              onBlur={() => handleBlur('email')}
              className={errors.email !== '' ? 'error' : ''}
            />
            {errors.email !== '' && <span className="error-message">{errors.email}</span>}
          </div>
          <div className="form-group">
            <label htmlFor="message">Message:</label>
            <textarea
              id="message"
              placeholder="Write Your Message Here..."
              value={message}
              onChange={(e) => handleInputChange(e, 'message')}
              onBlur={() => handleBlur('message')}
              className={errors.message !== '' ? 'error' : ''}
            />
            {errors.message !== '' && <span className="error-message">{errors.message}</span>}
          </div>
          <button type="submit">Submit</button>
        </form>
      )}
    </section>
  );
};

export default ContactForm;
