import React, { useState } from 'react';
import { FaArrowAltCircleRight, FaArrowAltCircleLeft } from 'react-icons/fa';
import '../style/__Main.css';
import bg1 from '../asset/bg.jpg';
import bg2 from '../asset/about-bg.jpg';

const SliderData = [
  {
    image: bg1,
    text: "Welcome to Suitmedia !"
  },
  {
    image: bg2,
    text: "This is a place where technology & creativity fused into digital chemistry"
  },
];

const Main = () => {
  const [current, setCurrent] = useState(0);
  const length = SliderData.length;

  const nextSlide = () => {
    setCurrent(current === length - 1 ? 0 : current + 1);
  };

  const prevSlide = () => {
    setCurrent(current === 0 ? length - 1 : current - 1);
  };

  if (!Array.isArray(SliderData) || SliderData.length <= 0) {
    return null;
  }

  return (
    <section className='slider'>
      <FaArrowAltCircleLeft className='left-arrow' onClick={prevSlide} />
      <FaArrowAltCircleRight className='right-arrow' onClick={nextSlide} />
      <div className='dots'>
        {SliderData.map((_, index) => (
          <span
            key={index}
            className={index === current ? 'dot active' : 'dot'}
            onClick={() => setCurrent(index)}
          ></span>
        ))}
      </div>
      {SliderData.map((slide, index) => {
        return (
          <div
            className={index === current ? 'slide active' : 'slide'}
            key={index}
          >
            {index === current && (
              <div>
                <img src={slide.image} alt='mainImage' className='image' />
                <div className='text'>{(slide.text).toUpperCase()}</div>
              </div>
            )}
          </div>
        );
      })}
    </section>
  );
};

export default Main;
