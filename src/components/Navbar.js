import React, { useState } from 'react';
import '../style/__Navbar.css';
import logo from '../asset/logo.png';

const Navbar = () => {
  const [showSubMenu, setShowSubMenu] = useState(false);
  const [showMenu, setShowMenu] = useState(false);

  const handleHover = () => {
    if (!showMenu) {
      setShowSubMenu(true);
    }
  };

  const handleLeave = () => {
    setShowSubMenu(false);
  };

  const toggleMenu = () => {
    setShowMenu(!showMenu);
    setShowSubMenu(false);
  };

  return (
    <nav>
      <div className="logo">
        <a href="#">
          <img src={logo} alt="logo" />
        </a>
      </div>
      <button className={`hamburger ${showMenu ? 'open' : ''}`} onClick={toggleMenu}>
        <span className="hamburger-icon"></span>
      </button>
      <div className={`navbar-menu ${showMenu ? 'open' : ''}`}>
        <ul className={`menu ${showMenu ? 'open' : ''}`}>
          <li onMouseEnter={handleHover} onMouseLeave={handleLeave}>
            <a href="#">About</a>
            {showSubMenu && (
              <ul className="submenu">
                <li>
                  <a href="#">History</a>
                </li>
                <li>
                  <a href="#">Vision Mission</a>
                </li>
              </ul>
            )}
          </li>
          <li>
            <a href="#">Our Work</a>
          </li>
          <li>
            <a href="#">Our Team</a>
          </li>
          <li>
            <a href="#">Contact</a>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;
