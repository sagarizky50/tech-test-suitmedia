import Navbar from './components/Navbar';
import Main from './components/Main';
import Values from './components/Values';
import Footer from './components/Footer';
import ContactForm from './components/ContactForm';
import './App.css';

function App() {
  return (
    <>
      <Navbar />
      <Main />
      <Values />
      <ContactForm />
      <Footer />
    </>
  );
}

export default App;
